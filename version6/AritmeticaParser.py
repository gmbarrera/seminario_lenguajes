# Generated from Aritmetica.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\27")
        buf.write("|\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3\2\6\2\30\n\2\r\2\16\2")
        buf.write("\31\3\3\3\3\3\3\3\3\3\3\3\3\5\3\"\n\3\3\4\3\4\3\4\3\4")
        buf.write("\3\5\3\5\3\5\3\5\6\5,\n\5\r\5\16\5-\3\5\3\5\6\5\62\n\5")
        buf.write("\r\5\16\5\63\5\5\66\n\5\3\5\3\5\3\6\3\6\3\6\3\6\6\6>\n")
        buf.write("\6\r\6\16\6?\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\7\7O\n\7\f\7\16\7R\13\7\3\b\3\b\5\bV\n\b\3")
        buf.write("\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\ta\n\t\f\t\16\td")
        buf.write("\13\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\7\no\n\n\f\n")
        buf.write("\16\nr\13\n\3\13\3\13\3\13\3\13\3\13\3\13\5\13z\n\13\3")
        buf.write("\13\2\5\f\20\22\f\2\4\6\b\n\f\16\20\22\24\2\2\2\u0084")
        buf.write("\2\27\3\2\2\2\4!\3\2\2\2\6#\3\2\2\2\b\'\3\2\2\2\n9\3\2")
        buf.write("\2\2\fC\3\2\2\2\16U\3\2\2\2\20W\3\2\2\2\22e\3\2\2\2\24")
        buf.write("y\3\2\2\2\26\30\5\4\3\2\27\26\3\2\2\2\30\31\3\2\2\2\31")
        buf.write("\27\3\2\2\2\31\32\3\2\2\2\32\3\3\2\2\2\33\"\5\20\t\2\34")
        buf.write("\"\5\6\4\2\35\"\7\24\2\2\36\"\5\f\7\2\37\"\5\b\5\2 \"")
        buf.write("\5\n\6\2!\33\3\2\2\2!\34\3\2\2\2!\35\3\2\2\2!\36\3\2\2")
        buf.write("\2!\37\3\2\2\2! \3\2\2\2\"\5\3\2\2\2#$\7\24\2\2$%\7\3")
        buf.write("\2\2%&\5\20\t\2&\7\3\2\2\2\'(\7\4\2\2()\5\f\7\2)+\7\5")
        buf.write("\2\2*,\5\4\3\2+*\3\2\2\2,-\3\2\2\2-+\3\2\2\2-.\3\2\2\2")
        buf.write(".\65\3\2\2\2/\61\7\6\2\2\60\62\5\4\3\2\61\60\3\2\2\2\62")
        buf.write("\63\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2\2\64\66\3\2\2\2")
        buf.write("\65/\3\2\2\2\65\66\3\2\2\2\66\67\3\2\2\2\678\7\7\2\28")
        buf.write("\t\3\2\2\29:\7\b\2\2:;\5\f\7\2;=\7\t\2\2<>\5\4\3\2=<\3")
        buf.write("\2\2\2>?\3\2\2\2?=\3\2\2\2?@\3\2\2\2@A\3\2\2\2AB\7\n\2")
        buf.write("\2B\13\3\2\2\2CD\b\7\1\2DE\5\16\b\2EF\7\23\2\2FG\5\16")
        buf.write("\b\2GP\3\2\2\2HI\f\4\2\2IJ\7\21\2\2JO\5\f\7\5KL\f\3\2")
        buf.write("\2LM\7\22\2\2MO\5\f\7\4NH\3\2\2\2NK\3\2\2\2OR\3\2\2\2")
        buf.write("PN\3\2\2\2PQ\3\2\2\2Q\r\3\2\2\2RP\3\2\2\2SV\5\20\t\2T")
        buf.write("V\7\24\2\2US\3\2\2\2UT\3\2\2\2V\17\3\2\2\2WX\b\t\1\2X")
        buf.write("Y\5\22\n\2Yb\3\2\2\2Z[\f\4\2\2[\\\7\13\2\2\\a\5\22\n\2")
        buf.write("]^\f\3\2\2^_\7\f\2\2_a\5\22\n\2`Z\3\2\2\2`]\3\2\2\2ad")
        buf.write("\3\2\2\2b`\3\2\2\2bc\3\2\2\2c\21\3\2\2\2db\3\2\2\2ef\b")
        buf.write("\n\1\2fg\5\24\13\2gp\3\2\2\2hi\f\4\2\2ij\7\r\2\2jo\5\24")
        buf.write("\13\2kl\f\3\2\2lm\7\16\2\2mo\5\24\13\2nh\3\2\2\2nk\3\2")
        buf.write("\2\2or\3\2\2\2pn\3\2\2\2pq\3\2\2\2q\23\3\2\2\2rp\3\2\2")
        buf.write("\2sz\7\25\2\2tz\7\24\2\2uv\7\17\2\2vw\5\20\t\2wx\7\20")
        buf.write("\2\2xz\3\2\2\2ys\3\2\2\2yt\3\2\2\2yu\3\2\2\2z\25\3\2\2")
        buf.write("\2\20\31!-\63\65?NPU`bnpy")
        return buf.getvalue()


class AritmeticaParser ( Parser ):

    grammarFileName = "Aritmetica.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'='", "'if'", "'then '", "'else '", "'fi'", 
                     "'while'", "'do'", "'done'", "'+'", "'-'", "'*'", "'/'", 
                     "'('", "')'", "'and'", "'or'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "AND_OPERATOR", 
                      "OR_OPERATOR", "COMPARATION_OPERATOR", "VARNAME", 
                      "NUMBER", "DIGIT", "WS" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_assignStatement = 2
    RULE_ifStatement = 3
    RULE_whileStatement = 4
    RULE_booleanExpression = 5
    RULE_operand = 6
    RULE_expression = 7
    RULE_term = 8
    RULE_factor = 9

    ruleNames =  [ "program", "statement", "assignStatement", "ifStatement", 
                   "whileStatement", "booleanExpression", "operand", "expression", 
                   "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    AND_OPERATOR=15
    OR_OPERATOR=16
    COMPARATION_OPERATOR=17
    VARNAME=18
    NUMBER=19
    DIGIT=20
    WS=21

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = AritmeticaParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 21 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 20
                self.statement()
                self.state = 23 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def assignStatement(self):
            return self.getTypedRuleContext(AritmeticaParser.AssignStatementContext,0)


        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def booleanExpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BooleanExpressionContext,0)


        def ifStatement(self):
            return self.getTypedRuleContext(AritmeticaParser.IfStatementContext,0)


        def whileStatement(self):
            return self.getTypedRuleContext(AritmeticaParser.WhileStatementContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritmeticaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 31
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 25
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 26
                self.assignStatement()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 27
                self.match(AritmeticaParser.VARNAME)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 28
                self.booleanExpression(0)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 29
                self.ifStatement()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 30
                self.whileStatement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_assignStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignStatement" ):
                listener.enterAssignStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignStatement" ):
                listener.exitAssignStatement(self)




    def assignStatement(self):

        localctx = AritmeticaParser.AssignStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_assignStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 33
            self.match(AritmeticaParser.VARNAME)
            self.state = 34
            self.match(AritmeticaParser.T__0)
            self.state = 35
            self.expression(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IfStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def booleanExpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BooleanExpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_ifStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIfStatement" ):
                listener.enterIfStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIfStatement" ):
                listener.exitIfStatement(self)




    def ifStatement(self):

        localctx = AritmeticaParser.IfStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_ifStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 37
            self.match(AritmeticaParser.T__1)
            self.state = 38
            self.booleanExpression(0)
            self.state = 39
            self.match(AritmeticaParser.T__2)
            self.state = 41 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 40
                self.statement()
                self.state = 43 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 51
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==AritmeticaParser.T__3:
                self.state = 45
                self.match(AritmeticaParser.T__3)
                self.state = 47 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 46
                    self.statement()
                    self.state = 49 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                        break



            self.state = 53
            self.match(AritmeticaParser.T__4)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WhileStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def booleanExpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BooleanExpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_whileStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhileStatement" ):
                listener.enterWhileStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhileStatement" ):
                listener.exitWhileStatement(self)




    def whileStatement(self):

        localctx = AritmeticaParser.WhileStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_whileStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 55
            self.match(AritmeticaParser.T__5)
            self.state = 56
            self.booleanExpression(0)
            self.state = 57
            self.match(AritmeticaParser.T__6)
            self.state = 59 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 58
                self.statement()
                self.state = 61 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__5) | (1 << AritmeticaParser.T__12) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 63
            self.match(AritmeticaParser.T__7)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BooleanExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.op = None # Token

        def operand(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.OperandContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.OperandContext,i)


        def COMPARATION_OPERATOR(self):
            return self.getToken(AritmeticaParser.COMPARATION_OPERATOR, 0)

        def booleanExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.BooleanExpressionContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.BooleanExpressionContext,i)


        def AND_OPERATOR(self):
            return self.getToken(AritmeticaParser.AND_OPERATOR, 0)

        def OR_OPERATOR(self):
            return self.getToken(AritmeticaParser.OR_OPERATOR, 0)

        def getRuleIndex(self):
            return AritmeticaParser.RULE_booleanExpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBooleanExpression" ):
                listener.enterBooleanExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBooleanExpression" ):
                listener.exitBooleanExpression(self)



    def booleanExpression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.BooleanExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 10
        self.enterRecursionRule(localctx, 10, self.RULE_booleanExpression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 66
            self.operand()
            self.state = 67
            localctx.op = self.match(AritmeticaParser.COMPARATION_OPERATOR)
            self.state = 68
            self.operand()
            self._ctx.stop = self._input.LT(-1)
            self.state = 78
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,7,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 76
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.BooleanExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_booleanExpression)
                        self.state = 70
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 71
                        localctx.op = self.match(AritmeticaParser.AND_OPERATOR)
                        self.state = 72
                        self.booleanExpression(3)
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.BooleanExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_booleanExpression)
                        self.state = 73
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 74
                        localctx.op = self.match(AritmeticaParser.OR_OPERATOR)
                        self.state = 75
                        self.booleanExpression(2)
                        pass

             
                self.state = 80
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,7,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class OperandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def getRuleIndex(self):
            return AritmeticaParser.RULE_operand

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperand" ):
                listener.enterOperand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperand" ):
                listener.exitOperand(self)




    def operand(self):

        localctx = AritmeticaParser.OperandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_operand)
        try:
            self.state = 83
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 81
                self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 82
                self.match(AritmeticaParser.VARNAME)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 14
        self.enterRecursionRule(localctx, 14, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 86
            self.term(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 96
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,10,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 94
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 88
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 89
                        self.match(AritmeticaParser.T__8)
                        self.state = 90
                        self.term(0)
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 91
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 92
                        self.match(AritmeticaParser.T__9)
                        self.state = 93
                        self.term(0)
                        pass

             
                self.state = 98
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,10,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self):
            return self.getTypedRuleContext(AritmeticaParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 16
        self.enterRecursionRule(localctx, 16, self.RULE_term, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 100
            self.factor()
            self._ctx.stop = self._input.LT(-1)
            self.state = 110
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,12,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 108
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 102
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 103
                        self.match(AritmeticaParser.T__10)
                        self.state = 104
                        self.factor()
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 105
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 106
                        self.match(AritmeticaParser.T__11)
                        self.state = 107
                        self.factor()
                        pass

             
                self.state = 112
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,12,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.n = None # Token
            self.vn = None # Token

        def NUMBER(self):
            return self.getToken(AritmeticaParser.NUMBER, 0)

        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = AritmeticaParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_factor)
        try:
            self.state = 119
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 113
                localctx.n = self.match(AritmeticaParser.NUMBER)
                pass
            elif token in [AritmeticaParser.VARNAME]:
                self.enterOuterAlt(localctx, 2)
                self.state = 114
                localctx.vn = self.match(AritmeticaParser.VARNAME)
                pass
            elif token in [AritmeticaParser.T__12]:
                self.enterOuterAlt(localctx, 3)
                self.state = 115
                self.match(AritmeticaParser.T__12)
                self.state = 116
                self.expression(0)
                self.state = 117
                self.match(AritmeticaParser.T__13)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[5] = self.booleanExpression_sempred
        self._predicates[7] = self.expression_sempred
        self._predicates[8] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def booleanExpression_sempred(self, localctx:BooleanExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 4:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 1)
         




