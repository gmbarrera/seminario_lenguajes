# Generated from Aritmetica.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\27")
        buf.write("}\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\3\3\3\3\3\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3")
        buf.write("\6\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\t")
        buf.write("\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17")
        buf.write("\3\17\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\22\5\22l\n\22\3\23\6\23o\n")
        buf.write("\23\r\23\16\23p\3\24\6\24t\n\24\r\24\16\24u\3\25\3\25")
        buf.write("\3\26\3\26\3\26\3\26\2\2\27\3\3\5\4\7\5\t\6\13\7\r\b\17")
        buf.write("\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23")
        buf.write("%\24\'\25)\26+\27\3\2\6\4\2>>@@\3\2c|\3\2\62;\5\2\13\f")
        buf.write("\17\17\"\"\2\u0082\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2")
        buf.write("\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21")
        buf.write("\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3")
        buf.write("\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2")
        buf.write("\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2")
        buf.write("\2\2\3-\3\2\2\2\5/\3\2\2\2\7\62\3\2\2\2\t8\3\2\2\2\13")
        buf.write(">\3\2\2\2\rA\3\2\2\2\17G\3\2\2\2\21J\3\2\2\2\23O\3\2\2")
        buf.write("\2\25Q\3\2\2\2\27S\3\2\2\2\31U\3\2\2\2\33W\3\2\2\2\35")
        buf.write("Y\3\2\2\2\37[\3\2\2\2!_\3\2\2\2#k\3\2\2\2%n\3\2\2\2\'")
        buf.write("s\3\2\2\2)w\3\2\2\2+y\3\2\2\2-.\7?\2\2.\4\3\2\2\2/\60")
        buf.write("\7k\2\2\60\61\7h\2\2\61\6\3\2\2\2\62\63\7v\2\2\63\64\7")
        buf.write("j\2\2\64\65\7g\2\2\65\66\7p\2\2\66\67\7\"\2\2\67\b\3\2")
        buf.write("\2\289\7g\2\29:\7n\2\2:;\7u\2\2;<\7g\2\2<=\7\"\2\2=\n")
        buf.write("\3\2\2\2>?\7h\2\2?@\7k\2\2@\f\3\2\2\2AB\7y\2\2BC\7j\2")
        buf.write("\2CD\7k\2\2DE\7n\2\2EF\7g\2\2F\16\3\2\2\2GH\7f\2\2HI\7")
        buf.write("q\2\2I\20\3\2\2\2JK\7f\2\2KL\7q\2\2LM\7p\2\2MN\7g\2\2")
        buf.write("N\22\3\2\2\2OP\7-\2\2P\24\3\2\2\2QR\7/\2\2R\26\3\2\2\2")
        buf.write("ST\7,\2\2T\30\3\2\2\2UV\7\61\2\2V\32\3\2\2\2WX\7*\2\2")
        buf.write("X\34\3\2\2\2YZ\7+\2\2Z\36\3\2\2\2[\\\7c\2\2\\]\7p\2\2")
        buf.write("]^\7f\2\2^ \3\2\2\2_`\7q\2\2`a\7t\2\2a\"\3\2\2\2bc\7?")
        buf.write("\2\2cl\7?\2\2de\7>\2\2el\7?\2\2fg\7@\2\2gl\7?\2\2hl\t")
        buf.write("\2\2\2ij\7#\2\2jl\7?\2\2kb\3\2\2\2kd\3\2\2\2kf\3\2\2\2")
        buf.write("kh\3\2\2\2ki\3\2\2\2l$\3\2\2\2mo\t\3\2\2nm\3\2\2\2op\3")
        buf.write("\2\2\2pn\3\2\2\2pq\3\2\2\2q&\3\2\2\2rt\5)\25\2sr\3\2\2")
        buf.write("\2tu\3\2\2\2us\3\2\2\2uv\3\2\2\2v(\3\2\2\2wx\t\4\2\2x")
        buf.write("*\3\2\2\2yz\t\5\2\2z{\3\2\2\2{|\b\26\2\2|,\3\2\2\2\6\2")
        buf.write("kpu\3\b\2\2")
        return buf.getvalue()


class AritmeticaLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    AND_OPERATOR = 15
    OR_OPERATOR = 16
    COMPARATION_OPERATOR = 17
    VARNAME = 18
    NUMBER = 19
    DIGIT = 20
    WS = 21

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'='", "'if'", "'then '", "'else '", "'fi'", "'while'", "'do'", 
            "'done'", "'+'", "'-'", "'*'", "'/'", "'('", "')'", "'and'", 
            "'or'" ]

    symbolicNames = [ "<INVALID>",
            "AND_OPERATOR", "OR_OPERATOR", "COMPARATION_OPERATOR", "VARNAME", 
            "NUMBER", "DIGIT", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "AND_OPERATOR", "OR_OPERATOR", "COMPARATION_OPERATOR", 
                  "VARNAME", "NUMBER", "DIGIT", "WS" ]

    grammarFileName = "Aritmetica.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


