from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream
from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener
import antlr4


class RealListener(AritmeticaListener):
    
    def __init__(self):
        self.variables = {}


    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        for node in ctx.children:
            self.visitStatement(node)


    def exitProgram(self, ctx:AritmeticaParser.ProgramContext):
        print('Program ended. :)')


    def visitStatement(self, ctx:AritmeticaParser.StatementContext):
        for node in ctx.children:
            if type(node) == AritmeticaParser.ExpressionContext:
                result = self.visitExpression(node)
                print(result)
            
            elif type(node) == AritmeticaParser.AssignStatementContext:
                self.visitAssignStatement(node)

            elif type(node) == antlr4.tree.Tree.TerminalNodeImpl:   # VARNAME
                result = self.variables[node.symbol.text]
                print(node.symbol.text + ' = ' + str(result))

            elif type(node) == AritmeticaParser.BooleanExpressionContext:
                result = self.visitBooleanExpresion(node)
                print(result)

            elif type(node) == AritmeticaParser.IfStatementContext:
                condition = self.visitBooleanExpresion(node.children[1])
                if condition:
                    i = 3
                    while i < len(node.children) - 1 and type(node.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
                        self.visitStatement(node.children[i])
                        i += 1
                else:
                    i = 3
                    while i < len(node.children) - 1 and type(node.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
                        i += 1
                        # sale del while cuando encuentra el ELSE
                    
                    i += 1      # Para saltear el ELSE
                    while i < len(node.children) - 1:
                        self.visitStatement(node.children[i])
                        i += 1

            elif type(node) == AritmeticaParser.WhileStatementContext:
                self.visitWhileStatement(node)


    def visitWhileStatement(self, ctx:AritmeticaParser.WhileStatementContext):
        
        while self.visitBooleanExpresion(ctx.children[1]):
            print('en el while!!!')
            i = 3
            while i < len(ctx.children) - 1 and type(ctx.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
                self.visitStatement(ctx.children[i])
                i += 1


    def visitIfStatement(self, ctx:AritmeticaParser.IfStatementContext):
        pass



    # Ok
    def visitBooleanExpresion(self, ctx:AritmeticaParser.BooleanExpressionContext):

        if ctx.op.text == 'and':
            be_left = self.visitBooleanExpresion(ctx.children[0])
            be_right = self.visitBooleanExpresion(ctx.children[2])
            return be_left and be_right

        if ctx.op.text == 'or':
            be_left = self.visitBooleanExpresion(ctx.children[0])
            be_right = self.visitBooleanExpresion(ctx.children[2])
            return be_left or be_right

        left =  self.visitOperand(ctx.children[0])
        right = self.visitOperand(ctx.children[2])
        op = ctx.children[1].symbol.text

        return eval(str(left) + op + str(right))

    # Ok
    def visitOperand(self, ctx:AritmeticaParser.OperandContext):
        node = ctx.children[0]
        if type(node) == AritmeticaParser.ExpressionContext:
            return self.visitExpression(node)

        if type(node) == antlr4.tree.Tree.TerminalNodeImpl:
            return self.variables[node.symbol.text]
        
        return ''   # No deberia pasar

    # Ok
    def visitAssignStatement(self, ctx:AritmeticaParser.AssignStatementContext):
        name = ctx.children[0].symbol.text
        value = self.visitExpression(ctx.children[2])
        self.variables[name] = value

    # Ok
    def visitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        if type(ctx.children[0]) == AritmeticaParser.TermContext:
            # Es un Term
            return self.visitTerm(ctx.children[0])
        
        # Es una Expression + - Term
        sign = 1
        if ctx.children[1].symbol.text == '-':
            sign = -1

        value1 = self.visitExpression(ctx.children[0])
        value2 = self.visitTerm(ctx.children[2])

        return value1 + sign * value2

    # Ok
    def visitTerm(self, ctx:AritmeticaParser.TermContext):
        if type(ctx.children[0]) == AritmeticaParser.FactorContext:
            # Es un factor
            return self.visitFactor(ctx.children[0])

        # Es un term * / factor
        value1 = self.visitTerm(ctx.children[0])
        value2 = self.visitFactor(ctx.children[2])

        if ctx.children[1].symbol.text == '*':
            return value1 * value2

        return value1 / value2

    # Ok
    def visitFactor(self, ctx:AritmeticaParser.FactorContext):
        
        if ctx.n != None:
            return int(ctx.n.text)

        if ctx.vn != None:
            return self.variables[ctx.vn.text]

        if ctx.children[0].symbol.text != '(':
            nodo = ctx.children[0]
            return int(nodo.symbol.text)

        return self.visitExpression(ctx.children[1])
        

def main():
  
    # program = "56 == 46 \n"
    # program += "6 == 3 \n"
    program = "a = 37-2*3+6  \n"
    program += "b = 54  \n"
    # program += " a == b \n"
    program += "c = (23 *2-4)*5+34-16/4  \n"
    program += "c <= 241  \n"
    program += "b   "
    program += "3+4-9*5  \n "
    program += "a   \n"
    program += "if 1==2 or a == 37 and 4 == 2+2*3  then " 
    program += "   a = 666 "

    program += "   if b == 54 then"
    program += "      z = 5568558 "
    program += "   fi  \n" 

    program += "   x = 999 "
    program += "else "
    program += "   a =1234 "
    program += "   y = 564 "
    program += "fi  \n" 



    program = " a = 0 "
    program += "while a < 10 do "
    program += "    a = a + 1 "
    program += "done "
    program += "a"


    input = InputStream(program)
    lexer = AritmeticaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = AritmeticaParser(stream)

    tree = parser.program()
    
    listener = RealListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
 
    print(listener.variables)

if __name__ == '__main__':
    main()