# Generated from Seminario.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\5")
        buf.write("\20\4\2\t\2\4\3\t\3\3\2\7\2\b\n\2\f\2\16\2\13\13\2\3\3")
        buf.write("\3\3\3\3\3\3\2\2\4\2\4\2\2\2\16\2\t\3\2\2\2\4\f\3\2\2")
        buf.write("\2\6\b\5\4\3\2\7\6\3\2\2\2\b\13\3\2\2\2\t\7\3\2\2\2\t")
        buf.write("\n\3\2\2\2\n\3\3\2\2\2\13\t\3\2\2\2\f\r\7\3\2\2\r\16\7")
        buf.write("\4\2\2\16\5\3\2\2\2\3\t")
        return buf.getvalue()


class SeminarioParser ( Parser ):

    grammarFileName = "Seminario.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'hello'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "ID", "WS" ]

    RULE_mi_prg = 0
    RULE_hi = 1

    ruleNames =  [ "mi_prg", "hi" ]

    EOF = Token.EOF
    T__0=1
    ID=2
    WS=3

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class Mi_prgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def hi(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SeminarioParser.HiContext)
            else:
                return self.getTypedRuleContext(SeminarioParser.HiContext,i)


        def getRuleIndex(self):
            return SeminarioParser.RULE_mi_prg

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMi_prg" ):
                listener.enterMi_prg(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMi_prg" ):
                listener.exitMi_prg(self)




    def mi_prg(self):

        localctx = SeminarioParser.Mi_prgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_mi_prg)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 7
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SeminarioParser.T__0:
                self.state = 4
                self.hi()
                self.state = 9
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class HiContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(SeminarioParser.ID, 0)

        def getRuleIndex(self):
            return SeminarioParser.RULE_hi

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterHi" ):
                listener.enterHi(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitHi" ):
                listener.exitHi(self)




    def hi(self):

        localctx = SeminarioParser.HiContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_hi)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 10
            self.match(SeminarioParser.T__0)
            self.state = 11
            self.match(SeminarioParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





