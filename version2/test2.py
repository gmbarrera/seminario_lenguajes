from antlr4 import CommonTokenStream, ParseTreeWalker, FileStream
from SeminarioLexer import SeminarioLexer
from SeminarioParser import SeminarioParser
from SeminarioListener import SeminarioListener

class SeminarioPrintListener(SeminarioListener):

    def enterMi_prg(self, ctx):
        print("PRG")
        
    def enterHi(self, ctx):
        print("Hello: %s" % ctx.ID())


def main():
    print('Comenzando...')

    input = FileStream('micodigo.sem')      # Cargo el codigo en un Stream
    lexer = SeminarioLexer(input)           # Genero el lexer para obtener los tokens del Stream generado
    stream = CommonTokenStream(lexer)       # Genero el Stream (facilitador, de dar) de los tokens de mi programa
    parser = SeminarioParser(stream)        # Genero el parser utilizando los tokens que vienen del stream de tokens
    tree = parser.mi_prg()                      # Genera el arbol de la regla 'hi'
    
    printer = SeminarioPrintListener()      # Creo el objeto encargado de visitar el nodo
    walker = ParseTreeWalker()              # Creo el objeto encargado de recorrer al arbol (AST)

    walker.walk(printer, tree)              # Recorro el arbol

    print('Proceso terminado. Buen dia.')
 

if __name__ == '__main__':
    main()