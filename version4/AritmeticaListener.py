# Generated from Aritmetica.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .AritmeticaParser import AritmeticaParser
else:
    from AritmeticaParser import AritmeticaParser


from DoItAll import DoItAll

helper = DoItAll();



# This class defines a complete listener for a parse tree produced by AritmeticaParser.
class AritmeticaListener(ParseTreeListener):

    # Enter a parse tree produced by AritmeticaParser#program.
    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#program.
    def exitProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#statement.
    def enterStatement(self, ctx:AritmeticaParser.StatementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#statement.
    def exitStatement(self, ctx:AritmeticaParser.StatementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#if_statement.
    def enterIf_statement(self, ctx:AritmeticaParser.If_statementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#if_statement.
    def exitIf_statement(self, ctx:AritmeticaParser.If_statementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#boolexpression.
    def enterBoolexpression(self, ctx:AritmeticaParser.BoolexpressionContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#boolexpression.
    def exitBoolexpression(self, ctx:AritmeticaParser.BoolexpressionContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#operand.
    def enterOperand(self, ctx:AritmeticaParser.OperandContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#operand.
    def exitOperand(self, ctx:AritmeticaParser.OperandContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#expression.
    def enterExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#expression.
    def exitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#term.
    def enterTerm(self, ctx:AritmeticaParser.TermContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#term.
    def exitTerm(self, ctx:AritmeticaParser.TermContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#factor.
    def enterFactor(self, ctx:AritmeticaParser.FactorContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#factor.
    def exitFactor(self, ctx:AritmeticaParser.FactorContext):
        pass


