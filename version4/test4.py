from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream
from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser


def main():
    program = "23 *  5 + 5 * (2 + 1) \n"
    program += "uno = 68+42 / 3      \n"
    program += "dos = 156      \n"
    program += "uno \n"
    program += "dos \n"
    program += "uno == dos \n"
    program += "(34+6*2) == 46 \n"

    program += "if dos <= 156 then uno fi  \n"

    input = InputStream(program)
    lexer = AritmeticaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = AritmeticaParser(stream)
    
    parser.program()                
 

if __name__ == '__main__':
    main()