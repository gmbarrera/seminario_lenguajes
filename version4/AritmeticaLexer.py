# Generated from Aritmetica.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



from DoItAll import DoItAll

helper = DoItAll();



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\25")
        buf.write("`\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\5\3\5\3\5\3\6\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3")
        buf.write("\n\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3")
        buf.write("\17\3\20\3\20\3\21\6\21R\n\21\r\21\16\21S\3\22\6\22W\n")
        buf.write("\22\r\22\16\22X\3\23\3\23\3\24\3\24\3\24\3\24\2\2\25\3")
        buf.write("\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16")
        buf.write("\33\17\35\20\37\21!\22#\23%\24\'\25\3\2\5\3\2c|\3\2\62")
        buf.write(";\5\2\13\f\17\17\"\"\2a\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3")
        buf.write("\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2")
        buf.write("\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2")
        buf.write("\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2")
        buf.write("!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\3)\3\2\2\2")
        buf.write("\5+\3\2\2\2\7.\3\2\2\2\t\64\3\2\2\2\13\67\3\2\2\2\r:\3")
        buf.write("\2\2\2\17<\3\2\2\2\21>\3\2\2\2\23A\3\2\2\2\25D\3\2\2\2")
        buf.write("\27F\3\2\2\2\31H\3\2\2\2\33J\3\2\2\2\35L\3\2\2\2\37N\3")
        buf.write("\2\2\2!Q\3\2\2\2#V\3\2\2\2%Z\3\2\2\2\'\\\3\2\2\2)*\7?")
        buf.write("\2\2*\4\3\2\2\2+,\7k\2\2,-\7h\2\2-\6\3\2\2\2./\7v\2\2")
        buf.write("/\60\7j\2\2\60\61\7g\2\2\61\62\7p\2\2\62\63\7\"\2\2\63")
        buf.write("\b\3\2\2\2\64\65\7h\2\2\65\66\7k\2\2\66\n\3\2\2\2\678")
        buf.write("\7?\2\289\7?\2\29\f\3\2\2\2:;\7>\2\2;\16\3\2\2\2<=\7@")
        buf.write("\2\2=\20\3\2\2\2>?\7>\2\2?@\7?\2\2@\22\3\2\2\2AB\7@\2")
        buf.write("\2BC\7?\2\2C\24\3\2\2\2DE\7-\2\2E\26\3\2\2\2FG\7/\2\2")
        buf.write("G\30\3\2\2\2HI\7,\2\2I\32\3\2\2\2JK\7\61\2\2K\34\3\2\2")
        buf.write("\2LM\7*\2\2M\36\3\2\2\2NO\7+\2\2O \3\2\2\2PR\t\2\2\2Q")
        buf.write("P\3\2\2\2RS\3\2\2\2SQ\3\2\2\2ST\3\2\2\2T\"\3\2\2\2UW\5")
        buf.write("%\23\2VU\3\2\2\2WX\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y$\3\2\2")
        buf.write("\2Z[\t\3\2\2[&\3\2\2\2\\]\t\4\2\2]^\3\2\2\2^_\b\24\2\2")
        buf.write("_(\3\2\2\2\5\2SX\3\b\2\2")
        return buf.getvalue()


class AritmeticaLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    VARNAME = 16
    NUMBER = 17
    DIGIT = 18
    WS = 19

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'='", "'if'", "'then '", "'fi'", "'=='", "'<'", "'>'", "'<='", 
            "'>='", "'+'", "'-'", "'*'", "'/'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "VARNAME", "NUMBER", "DIGIT", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "VARNAME", "NUMBER", "DIGIT", "WS" ]

    grammarFileName = "Aritmetica.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


