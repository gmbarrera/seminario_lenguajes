# Generated from Aritmetica.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



from DoItAll import DoItAll

helper = DoItAll();


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\25")
        buf.write("\u0083\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\3\2\6\2\24\n\2\r\2\16\2\25\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3&\n")
        buf.write("\3\3\4\3\4\3\4\3\4\6\4,\n\4\r\4\16\4-\3\4\3\4\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5K\n\5\3\6\3")
        buf.write("\6\3\6\3\6\3\6\5\6R\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7b\n\7\f\7\16\7e\13\7\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7")
        buf.write("\bu\n\b\f\b\16\bx\13\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t")
        buf.write("\u0081\n\t\3\t\2\4\f\16\n\2\4\6\b\n\f\16\20\2\2\2\u008a")
        buf.write("\2\23\3\2\2\2\4%\3\2\2\2\6\'\3\2\2\2\bJ\3\2\2\2\nQ\3\2")
        buf.write("\2\2\fS\3\2\2\2\16f\3\2\2\2\20\u0080\3\2\2\2\22\24\5\4")
        buf.write("\3\2\23\22\3\2\2\2\24\25\3\2\2\2\25\23\3\2\2\2\25\26\3")
        buf.write("\2\2\2\26\3\3\2\2\2\27\30\5\f\7\2\30\31\b\3\1\2\31&\3")
        buf.write("\2\2\2\32\33\7\22\2\2\33\34\7\3\2\2\34\35\5\f\7\2\35\36")
        buf.write("\b\3\1\2\36&\3\2\2\2\37 \7\22\2\2 &\b\3\1\2!\"\5\b\5\2")
        buf.write("\"#\b\3\1\2#&\3\2\2\2$&\5\6\4\2%\27\3\2\2\2%\32\3\2\2")
        buf.write("\2%\37\3\2\2\2%!\3\2\2\2%$\3\2\2\2&\5\3\2\2\2\'(\7\4\2")
        buf.write("\2()\5\b\5\2)+\7\5\2\2*,\5\4\3\2+*\3\2\2\2,-\3\2\2\2-")
        buf.write("+\3\2\2\2-.\3\2\2\2./\3\2\2\2/\60\7\6\2\2\60\7\3\2\2\2")
        buf.write("\61\62\5\n\6\2\62\63\7\7\2\2\63\64\5\n\6\2\64\65\b\5\1")
        buf.write("\2\65K\3\2\2\2\66\67\5\n\6\2\678\7\b\2\289\5\n\6\29:\b")
        buf.write("\5\1\2:K\3\2\2\2;<\5\n\6\2<=\7\t\2\2=>\5\n\6\2>?\b\5\1")
        buf.write("\2?K\3\2\2\2@A\5\n\6\2AB\7\n\2\2BC\5\n\6\2CD\b\5\1\2D")
        buf.write("K\3\2\2\2EF\5\n\6\2FG\7\13\2\2GH\5\n\6\2HI\b\5\1\2IK\3")
        buf.write("\2\2\2J\61\3\2\2\2J\66\3\2\2\2J;\3\2\2\2J@\3\2\2\2JE\3")
        buf.write("\2\2\2K\t\3\2\2\2LM\5\f\7\2MN\b\6\1\2NR\3\2\2\2OP\7\22")
        buf.write("\2\2PR\b\6\1\2QL\3\2\2\2QO\3\2\2\2R\13\3\2\2\2ST\b\7\1")
        buf.write("\2TU\5\16\b\2UV\b\7\1\2Vc\3\2\2\2WX\f\4\2\2XY\7\f\2\2")
        buf.write("YZ\5\16\b\2Z[\b\7\1\2[b\3\2\2\2\\]\f\3\2\2]^\7\r\2\2^")
        buf.write("_\5\16\b\2_`\b\7\1\2`b\3\2\2\2aW\3\2\2\2a\\\3\2\2\2be")
        buf.write("\3\2\2\2ca\3\2\2\2cd\3\2\2\2d\r\3\2\2\2ec\3\2\2\2fg\b")
        buf.write("\b\1\2gh\5\20\t\2hi\b\b\1\2iv\3\2\2\2jk\f\4\2\2kl\7\16")
        buf.write("\2\2lm\5\20\t\2mn\b\b\1\2nu\3\2\2\2op\f\3\2\2pq\7\17\2")
        buf.write("\2qr\5\20\t\2rs\b\b\1\2su\3\2\2\2tj\3\2\2\2to\3\2\2\2")
        buf.write("ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2w\17\3\2\2\2xv\3\2\2\2y")
        buf.write("z\7\23\2\2z\u0081\b\t\1\2{|\7\20\2\2|}\5\f\7\2}~\7\21")
        buf.write("\2\2~\177\b\t\1\2\177\u0081\3\2\2\2\u0080y\3\2\2\2\u0080")
        buf.write("{\3\2\2\2\u0081\21\3\2\2\2\f\25%-JQactv\u0080")
        return buf.getvalue()


class AritmeticaParser ( Parser ):

    grammarFileName = "Aritmetica.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'='", "'if'", "'then '", "'fi'", "'=='", 
                     "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", 
                     "'/'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "VARNAME", "NUMBER", "DIGIT", "WS" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_if_statement = 2
    RULE_boolexpression = 3
    RULE_operand = 4
    RULE_expression = 5
    RULE_term = 6
    RULE_factor = 7

    ruleNames =  [ "program", "statement", "if_statement", "boolexpression", 
                   "operand", "expression", "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    VARNAME=16
    NUMBER=17
    DIGIT=18
    WS=19

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = AritmeticaParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 16
                self.statement()
                self.state = 19 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__13) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None # ExpressionContext
            self._VARNAME = None # Token
            self.b = None # BoolexpressionContext

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def boolexpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BoolexpressionContext,0)


        def if_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.If_statementContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritmeticaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 35
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 21
                localctx.e = self.expression(0)
                print(localctx.e.value) 
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 24
                localctx._VARNAME = self.match(AritmeticaParser.VARNAME)
                self.state = 25
                self.match(AritmeticaParser.T__0)
                self.state = 26
                localctx.e = self.expression(0)
                helper.variables[(None if localctx._VARNAME is None else localctx._VARNAME.text)] = localctx.e.value 
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 29
                localctx._VARNAME = self.match(AritmeticaParser.VARNAME)
                print(helper.variables[(None if localctx._VARNAME is None else localctx._VARNAME.text)]) 
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 31
                localctx.b = self.boolexpression()
                print(localctx.b.value) 
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 34
                self.if_statement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class If_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def boolexpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BoolexpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_if_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_statement" ):
                listener.enterIf_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_statement" ):
                listener.exitIf_statement(self)




    def if_statement(self):

        localctx = AritmeticaParser.If_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_if_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 37
            self.match(AritmeticaParser.T__1)
            self.state = 38
            self.boolexpression()
            self.state = 39
            self.match(AritmeticaParser.T__2)
            self.state = 41 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 40
                self.statement()
                self.state = 43 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__13) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 45
            self.match(AritmeticaParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BoolexpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.o = None # OperandContext
            self.p = None # OperandContext

        def operand(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.OperandContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.OperandContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_boolexpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBoolexpression" ):
                listener.enterBoolexpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBoolexpression" ):
                listener.exitBoolexpression(self)




    def boolexpression(self):

        localctx = AritmeticaParser.BoolexpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_boolexpression)
        try:
            self.state = 72
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 47
                localctx.o = self.operand()
                self.state = 48
                self.match(AritmeticaParser.T__4)
                self.state = 49
                localctx.p = self.operand()
                localctx.value = localctx.o.value == localctx.p.value 
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 52
                localctx.o = self.operand()
                self.state = 53
                self.match(AritmeticaParser.T__5)
                self.state = 54
                localctx.p = self.operand()
                localctx.value = localctx.o.value < localctx.p.value 
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 57
                localctx.o = self.operand()
                self.state = 58
                self.match(AritmeticaParser.T__6)
                self.state = 59
                localctx.p = self.operand()
                localctx.value = localctx.o.value > localctx.p.value 
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 62
                localctx.o = self.operand()
                self.state = 63
                self.match(AritmeticaParser.T__7)
                self.state = 64
                localctx.p = self.operand()
                localctx.value = localctx.o.value <= localctx.p.value 
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 67
                localctx.o = self.operand()
                self.state = 68
                self.match(AritmeticaParser.T__8)
                self.state = 69
                localctx.p = self.operand()
                localctx.value = localctx.o.value >= localctx.p.value 
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class OperandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.e = None # ExpressionContext
            self._VARNAME = None # Token

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def getRuleIndex(self):
            return AritmeticaParser.RULE_operand

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperand" ):
                listener.enterOperand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperand" ):
                listener.exitOperand(self)




    def operand(self):

        localctx = AritmeticaParser.OperandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_operand)
        try:
            self.state = 79
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.T__13, AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 74
                localctx.e = self.expression(0)
                localctx.value = localctx.e.value 
                pass
            elif token in [AritmeticaParser.VARNAME]:
                self.enterOuterAlt(localctx, 2)
                self.state = 77
                localctx._VARNAME = self.match(AritmeticaParser.VARNAME)
                localctx.value = helper.variables[(None if localctx._VARNAME is None else localctx._VARNAME.text)]  
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.e = None # ExpressionContext
            self.t = None # TermContext

        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 10
        self.enterRecursionRule(localctx, 10, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 82
            localctx.t = self.term(0)
            localctx.value = localctx.t.value 
            self._ctx.stop = self._input.LT(-1)
            self.state = 97
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 95
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        localctx.e = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 85
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 86
                        self.match(AritmeticaParser.T__9)
                        self.state = 87
                        localctx.t = self.term(0)
                        localctx.value = localctx.e.value + localctx.t.value 
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        localctx.e = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 90
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 91
                        self.match(AritmeticaParser.T__10)
                        self.state = 92
                        localctx.t = self.term(0)
                        localctx.value = localctx.e.value - localctx.t.value 
                        pass

             
                self.state = 99
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.t = None # TermContext
            self.e = None # FactorContext

        def factor(self):
            return self.getTypedRuleContext(AritmeticaParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 12
        self.enterRecursionRule(localctx, 12, self.RULE_term, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 101
            localctx.e = self.factor()
            localctx.value = localctx.e.value 
            self._ctx.stop = self._input.LT(-1)
            self.state = 116
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 114
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        localctx.t = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 104
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 105
                        self.match(AritmeticaParser.T__11)
                        self.state = 106
                        localctx.e = self.factor()
                        localctx.value = localctx.t.value * localctx.e.value 
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        localctx.t = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 109
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 110
                        self.match(AritmeticaParser.T__12)
                        self.state = 111
                        localctx.e = self.factor()
                        localctx.value = localctx.t.value / localctx.e.value 
                        pass

             
                self.state = 118
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self._NUMBER = None # Token
            self.e = None # ExpressionContext

        def NUMBER(self):
            return self.getToken(AritmeticaParser.NUMBER, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = AritmeticaParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_factor)
        try:
            self.state = 126
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 119
                localctx._NUMBER = self.match(AritmeticaParser.NUMBER)
                localctx.value = int((None if localctx._NUMBER is None else localctx._NUMBER.text)) 
                pass
            elif token in [AritmeticaParser.T__13]:
                self.enterOuterAlt(localctx, 2)
                self.state = 121
                self.match(AritmeticaParser.T__13)
                self.state = 122
                localctx.e = self.expression(0)
                self.state = 123
                self.match(AritmeticaParser.T__14)
                localctx.value = localctx.e.value 
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[5] = self.expression_sempred
        self._predicates[6] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 1)
         




