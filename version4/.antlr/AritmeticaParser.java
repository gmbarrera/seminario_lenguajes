// Generated from e:\projects\seminario_lenguajes\version4\Aritmetica.g4 by ANTLR 4.7.1


from DoItAll import DoItAll

helper = DoItAll();


import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AritmeticaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, VARNAME=16, 
		NUMBER=17, DIGIT=18, WS=19;
	public static final int
		RULE_program = 0, RULE_statement = 1, RULE_if_statement = 2, RULE_boolexpression = 3, 
		RULE_operand = 4, RULE_expression = 5, RULE_term = 6, RULE_factor = 7;
	public static final String[] ruleNames = {
		"program", "statement", "if_statement", "boolexpression", "operand", "expression", 
		"term", "factor"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'='", "'if'", "'then '", "'fi'", "'=='", "'<'", "'>'", "'<='", 
		"'>='", "'+'", "'-'", "'*'", "'/'", "'('", "')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, "VARNAME", "NUMBER", "DIGIT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Aritmetica.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AritmeticaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(17); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(16);
				statement();
				}
				}
				setState(19); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__13) | (1L << VARNAME) | (1L << NUMBER))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public ExpressionContext e;
		public Token VARNAME;
		public BoolexpressionContext b;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode VARNAME() { return getToken(AritmeticaParser.VARNAME, 0); }
		public BoolexpressionContext boolexpression() {
			return getRuleContext(BoolexpressionContext.class,0);
		}
		public If_statementContext if_statement() {
			return getRuleContext(If_statementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			setState(35);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(21);
				((StatementContext)_localctx).e = expression(0);
				print(((StatementContext)_localctx).e.value) 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(24);
				((StatementContext)_localctx).VARNAME = match(VARNAME);
				setState(25);
				match(T__0);
				setState(26);
				((StatementContext)_localctx).e = expression(0);
				helper.variables[(((StatementContext)_localctx).VARNAME!=null?((StatementContext)_localctx).VARNAME.getText():null)] = ((StatementContext)_localctx).e.value 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(29);
				((StatementContext)_localctx).VARNAME = match(VARNAME);
				print(helper.variables[(((StatementContext)_localctx).VARNAME!=null?((StatementContext)_localctx).VARNAME.getText():null)]) 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(31);
				((StatementContext)_localctx).b = boolexpression();
				print(((StatementContext)_localctx).b.value) 
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(34);
				if_statement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_statementContext extends ParserRuleContext {
		public BoolexpressionContext boolexpression() {
			return getRuleContext(BoolexpressionContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public If_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_statement; }
	}

	public final If_statementContext if_statement() throws RecognitionException {
		If_statementContext _localctx = new If_statementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_if_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37);
			match(T__1);
			setState(38);
			boolexpression();
			setState(39);
			match(T__2);
			setState(41); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(40);
				statement();
				}
				}
				setState(43); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__13) | (1L << VARNAME) | (1L << NUMBER))) != 0) );
			setState(45);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolexpressionContext extends ParserRuleContext {
		public boolean value;
		public OperandContext o;
		public OperandContext p;
		public List<OperandContext> operand() {
			return getRuleContexts(OperandContext.class);
		}
		public OperandContext operand(int i) {
			return getRuleContext(OperandContext.class,i);
		}
		public BoolexpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolexpression; }
	}

	public final BoolexpressionContext boolexpression() throws RecognitionException {
		BoolexpressionContext _localctx = new BoolexpressionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_boolexpression);
		try {
			setState(72);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(47);
				((BoolexpressionContext)_localctx).o = operand();
				setState(48);
				match(T__4);
				setState(49);
				((BoolexpressionContext)_localctx).p = operand();
				_localctx.value = ((BoolexpressionContext)_localctx).o.value == ((BoolexpressionContext)_localctx).p.value 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(52);
				((BoolexpressionContext)_localctx).o = operand();
				setState(53);
				match(T__5);
				setState(54);
				((BoolexpressionContext)_localctx).p = operand();
				_localctx.value = ((BoolexpressionContext)_localctx).o.value < ((BoolexpressionContext)_localctx).p.value 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(57);
				((BoolexpressionContext)_localctx).o = operand();
				setState(58);
				match(T__6);
				setState(59);
				((BoolexpressionContext)_localctx).p = operand();
				_localctx.value = ((BoolexpressionContext)_localctx).o.value > ((BoolexpressionContext)_localctx).p.value 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(62);
				((BoolexpressionContext)_localctx).o = operand();
				setState(63);
				match(T__7);
				setState(64);
				((BoolexpressionContext)_localctx).p = operand();
				_localctx.value = ((BoolexpressionContext)_localctx).o.value <= ((BoolexpressionContext)_localctx).p.value 
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(67);
				((BoolexpressionContext)_localctx).o = operand();
				setState(68);
				match(T__8);
				setState(69);
				((BoolexpressionContext)_localctx).p = operand();
				_localctx.value = ((BoolexpressionContext)_localctx).o.value >= ((BoolexpressionContext)_localctx).p.value 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperandContext extends ParserRuleContext {
		public int value;
		public ExpressionContext e;
		public Token VARNAME;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode VARNAME() { return getToken(AritmeticaParser.VARNAME, 0); }
		public OperandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operand; }
	}

	public final OperandContext operand() throws RecognitionException {
		OperandContext _localctx = new OperandContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_operand);
		try {
			setState(79);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__13:
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(74);
				((OperandContext)_localctx).e = expression(0);
				_localctx.value = ((OperandContext)_localctx).e.value 
				}
				break;
			case VARNAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(77);
				((OperandContext)_localctx).VARNAME = match(VARNAME);
				_localctx.value = helper.variables[(((OperandContext)_localctx).VARNAME!=null?((OperandContext)_localctx).VARNAME.getText():null)]  
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public int value;
		public ExpressionContext e;
		public TermContext t;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 10;
		enterRecursionRule(_localctx, 10, RULE_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(82);
			((ExpressionContext)_localctx).t = term(0);
			_localctx.value = ((ExpressionContext)_localctx).t.value 
			}
			_ctx.stop = _input.LT(-1);
			setState(97);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(95);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						_localctx.e = _prevctx;
						_localctx.e = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(85);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(86);
						match(T__9);
						setState(87);
						((ExpressionContext)_localctx).t = term(0);
						_localctx.value = ((ExpressionContext)_localctx).e.value + ((ExpressionContext)_localctx).t.value 
						}
						break;
					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						_localctx.e = _prevctx;
						_localctx.e = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(90);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(91);
						match(T__10);
						setState(92);
						((ExpressionContext)_localctx).t = term(0);
						_localctx.value = ((ExpressionContext)_localctx).e.value - ((ExpressionContext)_localctx).t.value 
						}
						break;
					}
					} 
				}
				setState(99);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public int value;
		public TermContext t;
		public FactorContext e;
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	}

	public final TermContext term() throws RecognitionException {
		return term(0);
	}

	private TermContext term(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TermContext _localctx = new TermContext(_ctx, _parentState);
		TermContext _prevctx = _localctx;
		int _startState = 12;
		enterRecursionRule(_localctx, 12, RULE_term, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(101);
			((TermContext)_localctx).e = factor();
			_localctx.value = ((TermContext)_localctx).e.value 
			}
			_ctx.stop = _input.LT(-1);
			setState(116);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(114);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
					case 1:
						{
						_localctx = new TermContext(_parentctx, _parentState);
						_localctx.t = _prevctx;
						_localctx.t = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(104);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(105);
						match(T__11);
						setState(106);
						((TermContext)_localctx).e = factor();
						_localctx.value = ((TermContext)_localctx).t.value * ((TermContext)_localctx).e.value 
						}
						break;
					case 2:
						{
						_localctx = new TermContext(_parentctx, _parentState);
						_localctx.t = _prevctx;
						_localctx.t = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(109);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(110);
						match(T__12);
						setState(111);
						((TermContext)_localctx).e = factor();
						_localctx.value = ((TermContext)_localctx).t.value / ((TermContext)_localctx).e.value 
						}
						break;
					}
					} 
				}
				setState(118);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public int value;
		public Token NUMBER;
		public ExpressionContext e;
		public TerminalNode NUMBER() { return getToken(AritmeticaParser.NUMBER, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_factor);
		try {
			setState(126);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(119);
				((FactorContext)_localctx).NUMBER = match(NUMBER);
				_localctx.value = int((((FactorContext)_localctx).NUMBER!=null?((FactorContext)_localctx).NUMBER.getText():null)) 
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 2);
				{
				setState(121);
				match(T__13);
				setState(122);
				((FactorContext)_localctx).e = expression(0);
				setState(123);
				match(T__14);
				_localctx.value = ((FactorContext)_localctx).e.value 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 5:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		case 6:
			return term_sempred((TermContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean term_sempred(TermContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 2);
		case 3:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\25\u0083\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\6\2\24\n"+
		"\2\r\2\16\2\25\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\5\3&\n\3\3\4\3\4\3\4\3\4\6\4,\n\4\r\4\16\4-\3\4\3\4\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\5\5K\n\5\3\6\3\6\3\6\3\6\3\6\5\6R\n\6\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7b\n\7\f\7\16\7e\13\7\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\bu\n\b\f\b\16\bx\13\b"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u0081\n\t\3\t\2\4\f\16\n\2\4\6\b\n\f"+
		"\16\20\2\2\2\u008a\2\23\3\2\2\2\4%\3\2\2\2\6\'\3\2\2\2\bJ\3\2\2\2\nQ\3"+
		"\2\2\2\fS\3\2\2\2\16f\3\2\2\2\20\u0080\3\2\2\2\22\24\5\4\3\2\23\22\3\2"+
		"\2\2\24\25\3\2\2\2\25\23\3\2\2\2\25\26\3\2\2\2\26\3\3\2\2\2\27\30\5\f"+
		"\7\2\30\31\b\3\1\2\31&\3\2\2\2\32\33\7\22\2\2\33\34\7\3\2\2\34\35\5\f"+
		"\7\2\35\36\b\3\1\2\36&\3\2\2\2\37 \7\22\2\2 &\b\3\1\2!\"\5\b\5\2\"#\b"+
		"\3\1\2#&\3\2\2\2$&\5\6\4\2%\27\3\2\2\2%\32\3\2\2\2%\37\3\2\2\2%!\3\2\2"+
		"\2%$\3\2\2\2&\5\3\2\2\2\'(\7\4\2\2()\5\b\5\2)+\7\5\2\2*,\5\4\3\2+*\3\2"+
		"\2\2,-\3\2\2\2-+\3\2\2\2-.\3\2\2\2./\3\2\2\2/\60\7\6\2\2\60\7\3\2\2\2"+
		"\61\62\5\n\6\2\62\63\7\7\2\2\63\64\5\n\6\2\64\65\b\5\1\2\65K\3\2\2\2\66"+
		"\67\5\n\6\2\678\7\b\2\289\5\n\6\29:\b\5\1\2:K\3\2\2\2;<\5\n\6\2<=\7\t"+
		"\2\2=>\5\n\6\2>?\b\5\1\2?K\3\2\2\2@A\5\n\6\2AB\7\n\2\2BC\5\n\6\2CD\b\5"+
		"\1\2DK\3\2\2\2EF\5\n\6\2FG\7\13\2\2GH\5\n\6\2HI\b\5\1\2IK\3\2\2\2J\61"+
		"\3\2\2\2J\66\3\2\2\2J;\3\2\2\2J@\3\2\2\2JE\3\2\2\2K\t\3\2\2\2LM\5\f\7"+
		"\2MN\b\6\1\2NR\3\2\2\2OP\7\22\2\2PR\b\6\1\2QL\3\2\2\2QO\3\2\2\2R\13\3"+
		"\2\2\2ST\b\7\1\2TU\5\16\b\2UV\b\7\1\2Vc\3\2\2\2WX\f\4\2\2XY\7\f\2\2YZ"+
		"\5\16\b\2Z[\b\7\1\2[b\3\2\2\2\\]\f\3\2\2]^\7\r\2\2^_\5\16\b\2_`\b\7\1"+
		"\2`b\3\2\2\2aW\3\2\2\2a\\\3\2\2\2be\3\2\2\2ca\3\2\2\2cd\3\2\2\2d\r\3\2"+
		"\2\2ec\3\2\2\2fg\b\b\1\2gh\5\20\t\2hi\b\b\1\2iv\3\2\2\2jk\f\4\2\2kl\7"+
		"\16\2\2lm\5\20\t\2mn\b\b\1\2nu\3\2\2\2op\f\3\2\2pq\7\17\2\2qr\5\20\t\2"+
		"rs\b\b\1\2su\3\2\2\2tj\3\2\2\2to\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2"+
		"w\17\3\2\2\2xv\3\2\2\2yz\7\23\2\2z\u0081\b\t\1\2{|\7\20\2\2|}\5\f\7\2"+
		"}~\7\21\2\2~\177\b\t\1\2\177\u0081\3\2\2\2\u0080y\3\2\2\2\u0080{\3\2\2"+
		"\2\u0081\21\3\2\2\2\f\25%-JQactv\u0080";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}