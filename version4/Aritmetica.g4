grammar Aritmetica;

@header {

from DoItAll import DoItAll

helper = DoItAll();

}

program:
    (statement)+
    ;

statement:
    e=expression                {print($e.value) }
    | VARNAME '=' e=expression  {helper.variables[$VARNAME.text] = $e.value }
    | VARNAME                   {print(helper.variables[$VARNAME.text]) }
    | b=boolexpression          {print($b.value) }
    | if_statement
    ;

if_statement:
    'if' boolexpression 'then ' (statement)+ 'fi'           // Ahora al Listener!!!

    ;

boolexpression returns [boolean value]:
      o=operand '==' p=operand  {$value = $o.value == $p.value }
    | o=operand '<' p=operand   {$value = $o.value < $p.value }
    | o=operand '>' p=operand   {$value = $o.value > $p.value }
    | o=operand '<=' p=operand  {$value = $o.value <= $p.value }
    | o=operand '>=' p=operand  {$value = $o.value >= $p.value }
    ;

operand returns [int value]:
      e=expression                  {$value = $e.value }
    | VARNAME                       {$value = helper.variables[$VARNAME.text]  }
    ;

expression returns [int value]: 
    t=term                      {$value = $t.value }
    | e=expression '+' t=term   {$value = $e.value + $t.value }
    | e=expression '-' t=term   {$value = $e.value - $t.value }
    ;

term returns [int value]:   
    e=factor                    {$value = $e.value }
    | t=term '*' e=factor       {$value = $t.value * $e.value }
    | t=term '/' e=factor       {$value = $t.value / $e.value }
    ;

factor returns [int value]: 
    NUMBER                      {$value = int($NUMBER.text) }
    | '(' e=expression ')'      {$value = $e.value }
    ;

VARNAME : [a-z]+;

NUMBER : DIGIT+;
DIGIT  : [0-9];
WS : [ \r\n\t] -> skip; 
