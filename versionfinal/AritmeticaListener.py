# Generated from Aritmetica.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .AritmeticaParser import AritmeticaParser
else:
    from AritmeticaParser import AritmeticaParser

# This class defines a complete listener for a parse tree produced by AritmeticaParser.
class AritmeticaListener(ParseTreeListener):

    # Enter a parse tree produced by AritmeticaParser#program.
    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#program.
    def exitProgram(self, ctx:AritmeticaParser.ProgramContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#statement.
    def enterStatement(self, ctx:AritmeticaParser.StatementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#statement.
    def exitStatement(self, ctx:AritmeticaParser.StatementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#assignStatement.
    def enterAssignStatement(self, ctx:AritmeticaParser.AssignStatementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#assignStatement.
    def exitAssignStatement(self, ctx:AritmeticaParser.AssignStatementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#ifStatement.
    def enterIfStatement(self, ctx:AritmeticaParser.IfStatementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#ifStatement.
    def exitIfStatement(self, ctx:AritmeticaParser.IfStatementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#whileStatement.
    def enterWhileStatement(self, ctx:AritmeticaParser.WhileStatementContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#whileStatement.
    def exitWhileStatement(self, ctx:AritmeticaParser.WhileStatementContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#booleanExpression.
    def enterBooleanExpression(self, ctx:AritmeticaParser.BooleanExpressionContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#booleanExpression.
    def exitBooleanExpression(self, ctx:AritmeticaParser.BooleanExpressionContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#operand.
    def enterOperand(self, ctx:AritmeticaParser.OperandContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#operand.
    def exitOperand(self, ctx:AritmeticaParser.OperandContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#expression.
    def enterExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#expression.
    def exitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#term.
    def enterTerm(self, ctx:AritmeticaParser.TermContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#term.
    def exitTerm(self, ctx:AritmeticaParser.TermContext):
        pass


    # Enter a parse tree produced by AritmeticaParser#factor.
    def enterFactor(self, ctx:AritmeticaParser.FactorContext):
        pass

    # Exit a parse tree produced by AritmeticaParser#factor.
    def exitFactor(self, ctx:AritmeticaParser.FactorContext):
        pass


