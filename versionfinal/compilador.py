from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream
from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener
import antlr4
from ASMBuilder import ASMBuilder

class RealListener(AritmeticaListener):
    
    def __init__(self):
        self.step = 0
        self.variables = {}

        self.builder = ASMBuilder()
        self.asm_program = []

        self.stack = []


    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        for node in ctx.children:
            self.visitStatement(node)


    def exitProgram(self, ctx:AritmeticaParser.ProgramContext):
        program = []
        for instruction in self.asm_program:
            for line in instruction.split('\n'):
                if line.strip() != '':
                    program.append(line)

        correct_program = self.builder.correct_registers(program)

        print('ASM Program: ')
        for line in correct_program:
            print(line)
        


    def visitStatement(self, ctx:AritmeticaParser.StatementContext):
        for node in ctx.children:
            if type(node) == AritmeticaParser.ExpressionContext:
                result = self.visitExpression(node)
                print(result)
            
            elif type(node) == AritmeticaParser.AssignStatementContext:
                self.visitAssignStatement(node)

            elif type(node) == antlr4.tree.Tree.TerminalNodeImpl:   # VARNAME
                result = self.variables[node.symbol.text]
                print(node.symbol.text + ' = ' + str(result))

            elif type(node) == AritmeticaParser.BooleanExpressionContext:
                self.visitBooleanExpresion(node)

            elif type(node) == AritmeticaParser.IfStatementContext:
                self.visitIfStatement(node)

            elif type(node) == AritmeticaParser.WhileStatementContext:
                self.visitWhileStatement(node)


    def visitWhileStatement(self, node:AritmeticaParser.WhileStatementContext):

        while_label = self.builder.label_creator()
        self.asm_program.append(while_label + ':')

        while_condition = self.visitBooleanExpresion(node.children[1])
        asm, jump_label_t = self.builder.jumper_negate(while_condition)
        
        self.asm_program.append(asm)

        # While's Body
        i = 3
        while i < len(node.children) - 1 and type(node.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
            self.visitStatement(node.children[i])
            i += 1

        self.asm_program.append('JMP ' + while_label)
        self.asm_program.append(jump_label_t + ':')


    def visitIfStatement(self, node:AritmeticaParser.IfStatementContext):
        condition_operator = self.visitBooleanExpresion(node.children[1])

        asm, jump_label_t = self.builder.jumper_negate(condition_operator)
        self.asm_program.append(asm)

        # Cuerpo del IF
        i = 3
        while i < len(node.children) - 1 and type(node.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
            self.visitStatement(node.children[i])
            i += 1

        jump_label_f = self.builder.label_creator()
        self.asm_program.append('            JMP ' + jump_label_f)

        self.asm_program.append('            ' + jump_label_t + ':')

        # Instrucciones del ELSE
        i = 3
        while i < len(node.children) - 1 and type(node.children[i]) != antlr4.tree.Tree.TerminalNodeImpl:
            i += 1
            # sale del while cuando encuentra el ELSE
        
        i += 1      # Para saltear el ELSE
        while i < len(node.children) - 1:
            self.visitStatement(node.children[i])
            i += 1

        self.asm_program.append('            ' + jump_label_f + ':')


    def visitBooleanExpresion(self, ctx:AritmeticaParser.BooleanExpressionContext):
        if ctx.op.text == 'and':
            be_left = self.visitBooleanExpresion(ctx.children[0])
            be_right = self.visitBooleanExpresion(ctx.children[2])
            return be_left and be_right

        if ctx.op.text == 'or':
            be_left = self.visitBooleanExpresion(ctx.children[0])
            be_right = self.visitBooleanExpresion(ctx.children[2])
            return be_left or be_right

        left =  self.visitOperand(ctx.children[0])
        right = self.visitOperand(ctx.children[2])
        op = ctx.children[1].symbol.text

        self.asm_program.append(self.builder.compare(left, right))

        return op

    def visitOperand(self, ctx:AritmeticaParser.OperandContext):
        print(str(self.step) + "visitOperand")
        self.step += 1

        node = ctx.children[0]
        if type(node) == AritmeticaParser.ExpressionContext:
            return self.visitExpression(node)

        if type(node) == antlr4.tree.Tree.TerminalNodeImpl:
            return self.variables[node.symbol.text]
        
        return ''   # No deberia pasar

    def visitAssignStatement(self, ctx:AritmeticaParser.AssignStatementContext):
        print(str(self.step) + "visitAssignStatement")
        self.step += 1

        name = ctx.children[0].symbol.text
        value = self.visitExpression(ctx.children[2])
        

        self.asm_program.append(self.builder.assignment(name, value))
        

        self.variables[name] = value

    # Ok
    def visitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        print(str(self.step) + "visitExpression")
        self.step += 1

        if type(ctx.children[0]) == AritmeticaParser.TermContext:
            # Es un Term
            return self.visitTerm(ctx.children[0])
        
        # Es una Expression + - Term
        value1 = self.visitExpression(ctx.children[0])
        value2 = self.visitTerm(ctx.children[2])

        # Construir ASM suma
        if ctx.children[1].symbol.text == '-':
            asm, reg = self.builder.substract(value1, value2)
            self.asm_program.append(asm)
        else:
            asm, reg = self.builder.add(value1, value2)
            self.asm_program.append(asm)

        return reg

    # Ok
    def visitTerm(self, ctx:AritmeticaParser.TermContext):
        print(str(self.step) + "visitTerm")
        self.step += 1

        if type(ctx.children[0]) == AritmeticaParser.FactorContext:
            # Es un factor
            return self.visitFactor(ctx.children[0])

        # Es un term * / factor
        value1 = self.visitTerm(ctx.children[0])
        value2 = self.visitFactor(ctx.children[2])

        if ctx.children[1].symbol.text == '*':
            self.asm_program.append(self.builder.multiply(value1, value2))
            
            return value1 * value2

        return value1 / value2

    # Ok
    def visitFactor(self, ctx:AritmeticaParser.FactorContext):
        print(str(self.step) + "visitFactor")
        self.step += 1

        if ctx.n != None:
            return int(ctx.n.text)          # Es un numero, esta bien

        if ctx.vn != None:

            asm, reg = self.builder.get(ctx.vn.text)           # GET XX1, XX2
            self.asm_program.append(asm)

            return reg
            
        if ctx.children[0].symbol.text != '(':
            nodo = ctx.children[0]
            return int(nodo.symbol.text)

        return self.visitExpression(ctx.children[1])
        

def program1():
    '''
    MOV  AX, 'a'
    MOV  BX, 46
    PUT  AX, BX 
    '''
    return "a = 46\n"


def program2():
    '''
    MOV  AX, 46
    ADD  AX, 2
    '''
    return "46 + 2\n"


def program3():
    '''
    MOV  AX, 46
    ADD  AX, 2
    '''
    return "46 + 2 * 4\n"


def program4():
    '''
    MOV AX, 34
    MOV BX, 12
    CMP AX, BX
    '''
    return "34 == 12"

def program5():
    return "a = 3 + 1"

def program6():
    return "a = 3 + 1 \n b = 45 + 8"

def program7():
    return '''a = 3 + 1 \n 
              b = 45 + a  '''

def program8():
    return '''34 == 12+1'''

def program9():
    return '''a=12 \n
              34 == a'''

def program10():
    return '''if 22==12 then \n
                a=5 \n
              
              fi   \n
              b = 6'''

def program11():
    return '''
           a = 1   \n
           b = 10   \n
           while a < 5 do \n
              if a<3 then \n
                b=b+2 \n
              fi   \n
              a = a + 1  \n
           done
           c = a + b
           '''

def main():
    
    program = program11()

    input = InputStream(program)
    lexer = AritmeticaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = AritmeticaParser(stream)

    tree = parser.program()
    
    listener = RealListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
 

if __name__ == '__main__':
    main()
    