# Generated from Seminario.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SeminarioParser import SeminarioParser
else:
    from SeminarioParser import SeminarioParser

# This class defines a complete listener for a parse tree produced by SeminarioParser.
class SeminarioListener(ParseTreeListener):

    # Enter a parse tree produced by SeminarioParser#hi.
    def enterHi(self, ctx:SeminarioParser.HiContext):
        pass

    # Exit a parse tree produced by SeminarioParser#hi.
    def exitHi(self, ctx:SeminarioParser.HiContext):
        pass


