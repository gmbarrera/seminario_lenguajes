# Generated from PseudoASM.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\b")
        buf.write("\27\4\2\t\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\2\3\2\3\2\3\2\3\2\3\2\5\2\25\n\2\3\2\2\2\3\2\2\2\2\30")
        buf.write("\2\24\3\2\2\2\4\5\7\3\2\2\5\6\7\5\2\2\6\7\7\6\2\2\7\25")
        buf.write("\7\5\2\2\b\t\7\3\2\2\t\n\7\4\2\2\n\13\7\6\2\2\13\25\7")
        buf.write("\5\2\2\f\r\7\3\2\2\r\16\7\5\2\2\16\17\7\6\2\2\17\25\7")
        buf.write("\4\2\2\20\21\7\3\2\2\21\22\7\4\2\2\22\23\7\6\2\2\23\25")
        buf.write("\7\4\2\2\24\4\3\2\2\2\24\b\3\2\2\2\24\f\3\2\2\2\24\20")
        buf.write("\3\2\2\2\25\3\3\2\2\2\3\24")
        return buf.getvalue()


class PseudoASMParser ( Parser ):

    grammarFileName = "PseudoASM.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'MOV'", "<INVALID>", "<INVALID>", "','" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "MEMORY", "REGISTERS", "COMMA", 
                      "NUMBER", "DIGIT" ]

    RULE_mov = 0

    ruleNames =  [ "mov" ]

    EOF = Token.EOF
    T__0=1
    MEMORY=2
    REGISTERS=3
    COMMA=4
    NUMBER=5
    DIGIT=6

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class MovContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def REGISTERS(self, i:int=None):
            if i is None:
                return self.getTokens(PseudoASMParser.REGISTERS)
            else:
                return self.getToken(PseudoASMParser.REGISTERS, i)

        def COMMA(self):
            return self.getToken(PseudoASMParser.COMMA, 0)

        def MEMORY(self, i:int=None):
            if i is None:
                return self.getTokens(PseudoASMParser.MEMORY)
            else:
                return self.getToken(PseudoASMParser.MEMORY, i)

        def getRuleIndex(self):
            return PseudoASMParser.RULE_mov

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMov" ):
                listener.enterMov(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMov" ):
                listener.exitMov(self)




    def mov(self):

        localctx = PseudoASMParser.MovContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_mov)
        try:
            self.state = 18
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 2
                self.match(PseudoASMParser.T__0)
                self.state = 3
                self.match(PseudoASMParser.REGISTERS)
                self.state = 4
                self.match(PseudoASMParser.COMMA)
                self.state = 5
                self.match(PseudoASMParser.REGISTERS)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 6
                self.match(PseudoASMParser.T__0)
                self.state = 7
                self.match(PseudoASMParser.MEMORY)
                self.state = 8
                self.match(PseudoASMParser.COMMA)
                self.state = 9
                self.match(PseudoASMParser.REGISTERS)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 10
                self.match(PseudoASMParser.T__0)
                self.state = 11
                self.match(PseudoASMParser.REGISTERS)
                self.state = 12
                self.match(PseudoASMParser.COMMA)
                self.state = 13
                self.match(PseudoASMParser.MEMORY)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 14
                self.match(PseudoASMParser.T__0)
                self.state = 15
                self.match(PseudoASMParser.MEMORY)
                self.state = 16
                self.match(PseudoASMParser.COMMA)
                self.state = 17
                self.match(PseudoASMParser.MEMORY)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





