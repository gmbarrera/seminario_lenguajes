# Generated from PseudoASM.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\b")
        buf.write("+\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\5\4!\n\4\3\5\3\5\3\6\6\6&\n\6\r\6\16\6")
        buf.write("\'\3\7\3\7\2\2\b\3\3\5\4\7\5\t\6\13\7\r\b\3\2\3\3\2\62")
        buf.write(";\2/\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2")
        buf.write("\13\3\2\2\2\2\r\3\2\2\2\3\17\3\2\2\2\5\23\3\2\2\2\7 \3")
        buf.write("\2\2\2\t\"\3\2\2\2\13%\3\2\2\2\r)\3\2\2\2\17\20\7O\2\2")
        buf.write("\20\21\7Q\2\2\21\22\7X\2\2\22\4\3\2\2\2\23\24\7(\2\2\24")
        buf.write("\25\5\13\6\2\25\6\3\2\2\2\26\27\7C\2\2\27!\7Z\2\2\30\31")
        buf.write("\7D\2\2\31!\7Z\2\2\32\33\7E\2\2\33!\7Z\2\2\34\35\7F\2")
        buf.write("\2\35!\7Z\2\2\36\37\7k\2\2\37!\7r\2\2 \26\3\2\2\2 \30")
        buf.write("\3\2\2\2 \32\3\2\2\2 \34\3\2\2\2 \36\3\2\2\2!\b\3\2\2")
        buf.write("\2\"#\7.\2\2#\n\3\2\2\2$&\5\r\7\2%$\3\2\2\2&\'\3\2\2\2")
        buf.write("\'%\3\2\2\2\'(\3\2\2\2(\f\3\2\2\2)*\t\2\2\2*\16\3\2\2")
        buf.write("\2\5\2 \'\2")
        return buf.getvalue()


class PseudoASMLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    MEMORY = 2
    REGISTERS = 3
    COMMA = 4
    NUMBER = 5
    DIGIT = 6

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'MOV'", "','" ]

    symbolicNames = [ "<INVALID>",
            "MEMORY", "REGISTERS", "COMMA", "NUMBER", "DIGIT" ]

    ruleNames = [ "T__0", "MEMORY", "REGISTERS", "COMMA", "NUMBER", "DIGIT" ]

    grammarFileName = "PseudoASM.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


