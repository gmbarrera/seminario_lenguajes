// Generated from d:\projects\seminario_lenguajes\mini_vm\PseudoASM.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PseudoASMParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, MEMORY=6, REGISTER=7, COMMA=8, 
		NUMBER=9, DIGIT=10;
	public static final int
		RULE_mov = 0, RULE_push = 1, RULE_pop = 2, RULE_print = 3, RULE_input = 4;
	public static final String[] ruleNames = {
		"mov", "push", "pop", "print", "input"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'MOV'", "'PUSH'", "'POP'", "'PRINT'", "'INPUT'", null, null, "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "MEMORY", "REGISTER", "COMMA", "NUMBER", 
		"DIGIT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "PseudoASM.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public PseudoASMParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class MovContext extends ParserRuleContext {
		public List<TerminalNode> REGISTER() { return getTokens(PseudoASMParser.REGISTER); }
		public TerminalNode REGISTER(int i) {
			return getToken(PseudoASMParser.REGISTER, i);
		}
		public TerminalNode COMMA() { return getToken(PseudoASMParser.COMMA, 0); }
		public List<TerminalNode> MEMORY() { return getTokens(PseudoASMParser.MEMORY); }
		public TerminalNode MEMORY(int i) {
			return getToken(PseudoASMParser.MEMORY, i);
		}
		public MovContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mov; }
	}

	public final MovContext mov() throws RecognitionException {
		MovContext _localctx = new MovContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_mov);
		try {
			setState(26);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(10);
				match(T__0);
				setState(11);
				match(REGISTER);
				setState(12);
				match(COMMA);
				setState(13);
				match(REGISTER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(14);
				match(T__0);
				setState(15);
				match(MEMORY);
				setState(16);
				match(COMMA);
				setState(17);
				match(REGISTER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(18);
				match(T__0);
				setState(19);
				match(REGISTER);
				setState(20);
				match(COMMA);
				setState(21);
				match(MEMORY);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(22);
				match(T__0);
				setState(23);
				match(MEMORY);
				setState(24);
				match(COMMA);
				setState(25);
				match(MEMORY);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PushContext extends ParserRuleContext {
		public TerminalNode MEMORY() { return getToken(PseudoASMParser.MEMORY, 0); }
		public TerminalNode REGISTER() { return getToken(PseudoASMParser.REGISTER, 0); }
		public PushContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_push; }
	}

	public final PushContext push() throws RecognitionException {
		PushContext _localctx = new PushContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_push);
		try {
			setState(32);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(28);
				match(T__1);
				setState(29);
				match(MEMORY);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(30);
				match(T__1);
				setState(31);
				match(REGISTER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PopContext extends ParserRuleContext {
		public TerminalNode MEMORY() { return getToken(PseudoASMParser.MEMORY, 0); }
		public TerminalNode REGISTER() { return getToken(PseudoASMParser.REGISTER, 0); }
		public PopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pop; }
	}

	public final PopContext pop() throws RecognitionException {
		PopContext _localctx = new PopContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_pop);
		try {
			setState(38);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(34);
				match(T__2);
				setState(35);
				match(MEMORY);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(36);
				match(T__2);
				setState(37);
				match(REGISTER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public TerminalNode REGISTER() { return getToken(PseudoASMParser.REGISTER, 0); }
		public TerminalNode MEMORY() { return getToken(PseudoASMParser.MEMORY, 0); }
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_print);
		try {
			setState(44);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(40);
				match(T__3);
				setState(41);
				match(REGISTER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(42);
				match(T__3);
				setState(43);
				match(MEMORY);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InputContext extends ParserRuleContext {
		public TerminalNode MEMORY() { return getToken(PseudoASMParser.MEMORY, 0); }
		public InputContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_input; }
	}

	public final InputContext input() throws RecognitionException {
		InputContext _localctx = new InputContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_input);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(T__4);
			setState(47);
			match(MEMORY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\f\64\4\2\t\2\4\3"+
		"\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\5\2\35\n\2\3\3\3\3\3\3\3\3\5\3#\n\3\3\4\3\4\3\4"+
		"\3\4\5\4)\n\4\3\5\3\5\3\5\3\5\5\5/\n\5\3\6\3\6\3\6\3\6\2\2\7\2\4\6\b\n"+
		"\2\2\2\64\2\34\3\2\2\2\4\"\3\2\2\2\6(\3\2\2\2\b.\3\2\2\2\n\60\3\2\2\2"+
		"\f\r\7\3\2\2\r\16\7\t\2\2\16\17\7\n\2\2\17\35\7\t\2\2\20\21\7\3\2\2\21"+
		"\22\7\b\2\2\22\23\7\n\2\2\23\35\7\t\2\2\24\25\7\3\2\2\25\26\7\t\2\2\26"+
		"\27\7\n\2\2\27\35\7\b\2\2\30\31\7\3\2\2\31\32\7\b\2\2\32\33\7\n\2\2\33"+
		"\35\7\b\2\2\34\f\3\2\2\2\34\20\3\2\2\2\34\24\3\2\2\2\34\30\3\2\2\2\35"+
		"\3\3\2\2\2\36\37\7\4\2\2\37#\7\b\2\2 !\7\4\2\2!#\7\t\2\2\"\36\3\2\2\2"+
		"\" \3\2\2\2#\5\3\2\2\2$%\7\5\2\2%)\7\b\2\2&\'\7\5\2\2\')\7\t\2\2($\3\2"+
		"\2\2(&\3\2\2\2)\7\3\2\2\2*+\7\6\2\2+/\7\t\2\2,-\7\6\2\2-/\7\b\2\2.*\3"+
		"\2\2\2.,\3\2\2\2/\t\3\2\2\2\60\61\7\7\2\2\61\62\7\b\2\2\62\13\3\2\2\2"+
		"\6\34\"(.";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}