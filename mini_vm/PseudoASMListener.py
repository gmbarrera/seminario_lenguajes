# Generated from PseudoASM.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .PseudoASMParser import PseudoASMParser
else:
    from PseudoASMParser import PseudoASMParser

# This class defines a complete listener for a parse tree produced by PseudoASMParser.
class PseudoASMListener(ParseTreeListener):

    # Enter a parse tree produced by PseudoASMParser#mov.
    def enterMov(self, ctx:PseudoASMParser.MovContext):
        pass

    # Exit a parse tree produced by PseudoASMParser#mov.
    def exitMov(self, ctx:PseudoASMParser.MovContext):
        pass


