grammar PseudoASM;




mov :
      'MOV' REGISTER COMMA REGISTER
    | 'MOV' MEMORY COMMA REGISTER
    | 'MOV' REGISTER COMMA MEMORY
    | 'MOV' MEMORY COMMA MEMORY
    ;

push :
      'PUSH' MEMORY
    | 'PUSH' REGISTER
    ;



pop :
      'POP' MEMORY
    | 'POP' REGISTER
    ;

print:
      'PRINT' REGISTER
    | 'PRINT' MEMORY
    ;

input:
      'INPUT' MEMORY
    ;



MEMORY :
    '&' NUMBER
    ;

REGISTER :
      'AX'
    | 'BX'
    | 'CX'
    | 'DX'
    | 'ip'
    ;

COMMA : ',' ;

NUMBER : DIGIT+;

DIGIT  : [0-9];