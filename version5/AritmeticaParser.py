# Generated from Aritmetica.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\21")
        buf.write("V\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\3\2\6\2\24\n\2\r\2\16\2\25\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\5\3\37\n\3\3\4\3\4\3\4\3\4\6\4%\n\4\r\4")
        buf.write("\16\4&\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\5\6\61\n\6\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7<\n\7\f\7\16\7?\13")
        buf.write("\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\bJ\n\b\f\b\16")
        buf.write("\bM\13\b\3\t\3\t\3\t\3\t\3\t\5\tT\n\t\3\t\2\4\f\16\n\2")
        buf.write("\4\6\b\n\f\16\20\2\2\2Y\2\23\3\2\2\2\4\36\3\2\2\2\6 \3")
        buf.write("\2\2\2\b*\3\2\2\2\n\60\3\2\2\2\f\62\3\2\2\2\16@\3\2\2")
        buf.write("\2\20S\3\2\2\2\22\24\5\4\3\2\23\22\3\2\2\2\24\25\3\2\2")
        buf.write("\2\25\23\3\2\2\2\25\26\3\2\2\2\26\3\3\2\2\2\27\37\5\f")
        buf.write("\7\2\30\31\7\16\2\2\31\32\7\3\2\2\32\37\5\f\7\2\33\37")
        buf.write("\7\16\2\2\34\37\5\b\5\2\35\37\5\6\4\2\36\27\3\2\2\2\36")
        buf.write("\30\3\2\2\2\36\33\3\2\2\2\36\34\3\2\2\2\36\35\3\2\2\2")
        buf.write("\37\5\3\2\2\2 !\7\4\2\2!\"\5\b\5\2\"$\7\5\2\2#%\5\4\3")
        buf.write("\2$#\3\2\2\2%&\3\2\2\2&$\3\2\2\2&\'\3\2\2\2\'(\3\2\2\2")
        buf.write("()\7\6\2\2)\7\3\2\2\2*+\5\n\6\2+,\7\r\2\2,-\5\n\6\2-\t")
        buf.write("\3\2\2\2.\61\5\f\7\2/\61\7\16\2\2\60.\3\2\2\2\60/\3\2")
        buf.write("\2\2\61\13\3\2\2\2\62\63\b\7\1\2\63\64\5\16\b\2\64=\3")
        buf.write("\2\2\2\65\66\f\4\2\2\66\67\7\7\2\2\67<\5\16\b\289\f\3")
        buf.write("\2\29:\7\b\2\2:<\5\16\b\2;\65\3\2\2\2;8\3\2\2\2<?\3\2")
        buf.write("\2\2=;\3\2\2\2=>\3\2\2\2>\r\3\2\2\2?=\3\2\2\2@A\b\b\1")
        buf.write("\2AB\5\20\t\2BK\3\2\2\2CD\f\4\2\2DE\7\t\2\2EJ\5\20\t\2")
        buf.write("FG\f\3\2\2GH\7\n\2\2HJ\5\20\t\2IC\3\2\2\2IF\3\2\2\2JM")
        buf.write("\3\2\2\2KI\3\2\2\2KL\3\2\2\2L\17\3\2\2\2MK\3\2\2\2NT\7")
        buf.write("\17\2\2OP\7\13\2\2PQ\5\f\7\2QR\7\f\2\2RT\3\2\2\2SN\3\2")
        buf.write("\2\2SO\3\2\2\2T\21\3\2\2\2\13\25\36&\60;=IKS")
        return buf.getvalue()


class AritmeticaParser ( Parser ):

    grammarFileName = "Aritmetica.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'='", "'if'", "'then '", "'fi'", "'+'", 
                     "'-'", "'*'", "'/'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "COMPARATION_OPERATOR", 
                      "VARNAME", "NUMBER", "DIGIT", "WS" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_if_statement = 2
    RULE_boolexpression = 3
    RULE_operand = 4
    RULE_expression = 5
    RULE_term = 6
    RULE_factor = 7

    ruleNames =  [ "program", "statement", "if_statement", "boolexpression", 
                   "operand", "expression", "term", "factor" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    COMPARATION_OPERATOR=11
    VARNAME=12
    NUMBER=13
    DIGIT=14
    WS=15

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = AritmeticaParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 16
                self.statement()
                self.state = 19 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__8) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None # ExpressionContext
            self.b = None # BoolexpressionContext

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def boolexpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BoolexpressionContext,0)


        def if_statement(self):
            return self.getTypedRuleContext(AritmeticaParser.If_statementContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritmeticaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 28
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 21
                localctx.e = self.expression(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 22
                self.match(AritmeticaParser.VARNAME)
                self.state = 23
                self.match(AritmeticaParser.T__0)
                self.state = 24
                localctx.e = self.expression(0)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 25
                self.match(AritmeticaParser.VARNAME)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 26
                localctx.b = self.boolexpression()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 27
                self.if_statement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class If_statementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def boolexpression(self):
            return self.getTypedRuleContext(AritmeticaParser.BoolexpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.StatementContext,i)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_if_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_statement" ):
                listener.enterIf_statement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_statement" ):
                listener.exitIf_statement(self)




    def if_statement(self):

        localctx = AritmeticaParser.If_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_if_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 30
            self.match(AritmeticaParser.T__1)
            self.state = 31
            self.boolexpression()
            self.state = 32
            self.match(AritmeticaParser.T__2)
            self.state = 34 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 33
                self.statement()
                self.state = 36 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << AritmeticaParser.T__1) | (1 << AritmeticaParser.T__8) | (1 << AritmeticaParser.VARNAME) | (1 << AritmeticaParser.NUMBER))) != 0)):
                    break

            self.state = 38
            self.match(AritmeticaParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BoolexpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None

        def operand(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritmeticaParser.OperandContext)
            else:
                return self.getTypedRuleContext(AritmeticaParser.OperandContext,i)


        def COMPARATION_OPERATOR(self):
            return self.getToken(AritmeticaParser.COMPARATION_OPERATOR, 0)

        def getRuleIndex(self):
            return AritmeticaParser.RULE_boolexpression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBoolexpression" ):
                listener.enterBoolexpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBoolexpression" ):
                listener.exitBoolexpression(self)




    def boolexpression(self):

        localctx = AritmeticaParser.BoolexpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_boolexpression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 40
            self.operand()
            self.state = 41
            self.match(AritmeticaParser.COMPARATION_OPERATOR)
            self.state = 42
            self.operand()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class OperandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.e = None # ExpressionContext

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def VARNAME(self):
            return self.getToken(AritmeticaParser.VARNAME, 0)

        def getRuleIndex(self):
            return AritmeticaParser.RULE_operand

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOperand" ):
                listener.enterOperand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOperand" ):
                listener.exitOperand(self)




    def operand(self):

        localctx = AritmeticaParser.OperandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_operand)
        try:
            self.state = 46
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.T__8, AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 44
                localctx.e = self.expression(0)
                pass
            elif token in [AritmeticaParser.VARNAME]:
                self.enterOuterAlt(localctx, 2)
                self.state = 45
                self.match(AritmeticaParser.VARNAME)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.e = None # ExpressionContext
            self.t = None # TermContext

        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 10
        self.enterRecursionRule(localctx, 10, self.RULE_expression, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 49
            localctx.t = self.term(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 59
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 57
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        localctx.e = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 51
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 52
                        self.match(AritmeticaParser.T__4)
                        self.state = 53
                        localctx.t = self.term(0)
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.ExpressionContext(self, _parentctx, _parentState)
                        localctx.e = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 54
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 55
                        self.match(AritmeticaParser.T__5)
                        self.state = 56
                        localctx.t = self.term(0)
                        pass

             
                self.state = 61
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.t = None # TermContext
            self.e = None # FactorContext

        def factor(self):
            return self.getTypedRuleContext(AritmeticaParser.FactorContext,0)


        def term(self):
            return self.getTypedRuleContext(AritmeticaParser.TermContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)



    def term(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritmeticaParser.TermContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 12
        self.enterRecursionRule(localctx, 12, self.RULE_term, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 63
            localctx.e = self.factor()
            self._ctx.stop = self._input.LT(-1)
            self.state = 73
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,7,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 71
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
                    if la_ == 1:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        localctx.t = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 65
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 66
                        self.match(AritmeticaParser.T__6)
                        self.state = 67
                        localctx.e = self.factor()
                        pass

                    elif la_ == 2:
                        localctx = AritmeticaParser.TermContext(self, _parentctx, _parentState)
                        localctx.t = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_term)
                        self.state = 68
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 69
                        self.match(AritmeticaParser.T__7)
                        self.state = 70
                        localctx.e = self.factor()
                        pass

             
                self.state = 75
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,7,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.value = None
            self.e = None # ExpressionContext

        def NUMBER(self):
            return self.getToken(AritmeticaParser.NUMBER, 0)

        def expression(self):
            return self.getTypedRuleContext(AritmeticaParser.ExpressionContext,0)


        def getRuleIndex(self):
            return AritmeticaParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)




    def factor(self):

        localctx = AritmeticaParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_factor)
        try:
            self.state = 81
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [AritmeticaParser.NUMBER]:
                self.enterOuterAlt(localctx, 1)
                self.state = 76
                self.match(AritmeticaParser.NUMBER)
                pass
            elif token in [AritmeticaParser.T__8]:
                self.enterOuterAlt(localctx, 2)
                self.state = 77
                self.match(AritmeticaParser.T__8)
                self.state = 78
                localctx.e = self.expression(0)
                self.state = 79
                self.match(AritmeticaParser.T__9)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[5] = self.expression_sempred
        self._predicates[6] = self.term_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def term_sempred(self, localctx:TermContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 1)
         




