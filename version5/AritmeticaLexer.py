# Generated from Aritmetica.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\21")
        buf.write("V\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\3\3\3\3\3\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3")
        buf.write("\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\f\3\f\5\fE\n\f\3\r\6\rH\n\r\r\r\16\rI\3\16\6\16M\n\16")
        buf.write("\r\16\16\16N\3\17\3\17\3\20\3\20\3\20\3\20\2\2\21\3\3")
        buf.write("\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16")
        buf.write("\33\17\35\20\37\21\3\2\6\4\2>>@@\3\2c|\3\2\62;\5\2\13")
        buf.write("\f\17\17\"\"\2[\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2")
        buf.write("\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21")
        buf.write("\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3")
        buf.write("\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\3!\3\2\2")
        buf.write("\2\5#\3\2\2\2\7&\3\2\2\2\t,\3\2\2\2\13/\3\2\2\2\r\61\3")
        buf.write("\2\2\2\17\63\3\2\2\2\21\65\3\2\2\2\23\67\3\2\2\2\259\3")
        buf.write("\2\2\2\27D\3\2\2\2\31G\3\2\2\2\33L\3\2\2\2\35P\3\2\2\2")
        buf.write("\37R\3\2\2\2!\"\7?\2\2\"\4\3\2\2\2#$\7k\2\2$%\7h\2\2%")
        buf.write("\6\3\2\2\2&\'\7v\2\2\'(\7j\2\2()\7g\2\2)*\7p\2\2*+\7\"")
        buf.write("\2\2+\b\3\2\2\2,-\7h\2\2-.\7k\2\2.\n\3\2\2\2/\60\7-\2")
        buf.write("\2\60\f\3\2\2\2\61\62\7/\2\2\62\16\3\2\2\2\63\64\7,\2")
        buf.write("\2\64\20\3\2\2\2\65\66\7\61\2\2\66\22\3\2\2\2\678\7*\2")
        buf.write("\28\24\3\2\2\29:\7+\2\2:\26\3\2\2\2;<\7?\2\2<E\7?\2\2")
        buf.write("=>\7>\2\2>E\7?\2\2?@\7@\2\2@E\7?\2\2AE\t\2\2\2BC\7#\2")
        buf.write("\2CE\7?\2\2D;\3\2\2\2D=\3\2\2\2D?\3\2\2\2DA\3\2\2\2DB")
        buf.write("\3\2\2\2E\30\3\2\2\2FH\t\3\2\2GF\3\2\2\2HI\3\2\2\2IG\3")
        buf.write("\2\2\2IJ\3\2\2\2J\32\3\2\2\2KM\5\35\17\2LK\3\2\2\2MN\3")
        buf.write("\2\2\2NL\3\2\2\2NO\3\2\2\2O\34\3\2\2\2PQ\t\4\2\2Q\36\3")
        buf.write("\2\2\2RS\t\5\2\2ST\3\2\2\2TU\b\20\2\2U \3\2\2\2\6\2DI")
        buf.write("N\3\b\2\2")
        return buf.getvalue()


class AritmeticaLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    COMPARATION_OPERATOR = 11
    VARNAME = 12
    NUMBER = 13
    DIGIT = 14
    WS = 15

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'='", "'if'", "'then '", "'fi'", "'+'", "'-'", "'*'", "'/'", 
            "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "COMPARATION_OPERATOR", "VARNAME", "NUMBER", "DIGIT", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "COMPARATION_OPERATOR", "VARNAME", 
                  "NUMBER", "DIGIT", "WS" ]

    grammarFileName = "Aritmetica.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


