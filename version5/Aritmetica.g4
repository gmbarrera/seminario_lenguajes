grammar Aritmetica;

program:
    (statement)+
    ;

statement:
    e=expression                
    | VARNAME '=' e=expression  
    | VARNAME                   
    | b=boolexpression          
    | if_statement
    ;

if_statement:
    'if' boolexpression 'then ' (statement)+ 'fi'           // Ahora al Listener!!!

    ;

boolexpression returns [boolean value]:
    operand COMPARATION_OPERATOR operand  
    ;

operand returns [int value]:
      e=expression                 
    | VARNAME                      
    ;

expression returns [int value]: 
    t=term                      
    | e=expression '+' t=term   
    | e=expression '-' t=term   
    ;

term returns [int value]:   
    e=factor                    
    | t=term '*' e=factor       
    | t=term '/' e=factor       
    ;

factor returns [int value]: 
    NUMBER                     
    | '(' e=expression ')'     
    ;


COMPARATION_OPERATOR : '=='|'<='|'>='|'<'|'>'|'!=';

VARNAME : [a-z]+;

NUMBER : DIGIT+;
DIGIT  : [0-9];
WS : [ \r\n\t] -> skip; 
