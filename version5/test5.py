from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream
from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser
from AritmeticaListener import AritmeticaListener
import antlr4

class PrintListener(AritmeticaListener):
    step = 0
    
    def enterProgram(self, ctx:AritmeticaParser.ProgramContext):
        print("Enter Program " + str(self.step))
        self.step += 1

    def exitProgram(self, ctx:AritmeticaParser.ProgramContext):
        print("Exit Program " + str(self.step))
        self.step += 1

    def enterStatement(self, ctx:AritmeticaParser.StatementContext):
        print("Enter Statement " + str(self.step))
        self.step += 1

    def exitStatement(self, ctx:AritmeticaParser.StatementContext):
        print("Exit Statement " + str(self.step))
        self.step += 1

    def enterBoolexpression(self, ctx:AritmeticaParser.BoolexpressionContext):
        print('Enter Boolean expression ' + str(self.step))
        self.step += 1
        


    def exitBoolexpression(self, ctx:AritmeticaParser.BoolexpressionContext):
        print('Exit Boolean expression ' + str(self.step))
        self.step += 1
        
        for node in ctx.children:
            if type(node) == antlr4.tree.Tree.TerminalNodeImpl:
                print(node)

    def enterOperand(self, ctx:AritmeticaParser.OperandContext):
        print('Enter Operand ' + str(self.step))
        self.step += 1

    def exitOperand(self, ctx:AritmeticaParser.OperandContext):
        print('Exit Operand ' + str(self.step))
        self.step += 1

    def enterExpression(self, ctx:AritmeticaParser.ExpressionContext):
        print('Enter Expression ' + str(self.step))
        self.step += 1

    def exitExpression(self, ctx:AritmeticaParser.ExpressionContext):
        print('Exit Expression ' + str(self.step))
        self.step += 1

    def enterTerm(self, ctx:AritmeticaParser.TermContext):
        print('Enter Term ' + str(self.step))
        self.step += 1

    def exitTerm(self, ctx:AritmeticaParser.TermContext):
        print('Exit Term ' + str(self.step))
        self.step += 1

    def enterFactor(self, ctx:AritmeticaParser.FactorContext):
        print('Enter Factor ' + str(self.step))
        self.step += 1

        # Visitar cada uno de los children
        print(ctx.children[0])

    def exitFactor(self, ctx:AritmeticaParser.FactorContext):
        print('Exit Factor ' + str(self.step))
        self.step += 1




def main():
  
    program = "56 == 46 \n"

    input = InputStream(program)
    lexer = AritmeticaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = AritmeticaParser(stream)

    tree = parser.program()
    
    listener = PrintListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
 

if __name__ == '__main__':
    main()