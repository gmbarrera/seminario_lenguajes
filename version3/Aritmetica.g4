grammar Aritmetica;

statement:
    e=expression                {print($e.value) }
    ;

expression returns [int value]: 
    t=term                      {$value = $t.value }
    | e=expression '+' t=term   {$value = $e.value + $t.value }
    ;

term returns [int value]:   
    e=factor                    {$value = $e.value }
    | t=term '*' e=factor       {$value = $t.value * $e.value }
    ;

factor returns [int value]: 
    NUMBER                      {$value = int($NUMBER.text) }
    | '(' e=expression ')'      {$value = $e.value }
    ;

NUMBER : DIGIT+;
DIGIT  : [0-9];
WS : [ \r\n\t] -> skip; 
