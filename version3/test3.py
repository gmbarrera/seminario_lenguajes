from antlr4 import CommonTokenStream, ParseTreeWalker, InputStream
from AritmeticaLexer import AritmeticaLexer
from AritmeticaParser import AritmeticaParser


def main():
    input = InputStream("23*5 +5 * ( 2+1)")
    lexer = AritmeticaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = AritmeticaParser(stream)
    
    parser.statement()                
 

if __name__ == '__main__':
    main()