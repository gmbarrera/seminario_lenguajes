# TP 1

### Fecha de entrega: 11-Octubre-2018

Realizar la gramática para operar con conjuntos.

## Conjunto

1. Establecer la forma de crear conjunto numericos.
2. Establecer el conjunto de referencia (o universal)


### Ejemplo:
    A = conjunto[0..5]      //Define el conjunto {0,1,2,3,4,5}
    B = conjunto[0..10, 2]  //Define el conjunto {0,2,4,6,8,10}

## Operaciones:

1. Intersección
2. Unión
3. Diferencia
4. Complemento
5. Asignación
6. Mostrar
7. Pertenece Elemento
8. Sumar Elementos
9. Promediar Elementos
10. Longitud

### Ejemplos:

    A = set[1,10,2]     // {1,3,5,7,9}
    A.belong(3)         // Return True
    C = A.intersect(B)
    o
    C = A inter B
    A                   // Muestra el conjunto
    res = A.sum
    

